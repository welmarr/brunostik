<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commissions', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->float('amount');
            // PENDIND = 0
            // DONE = 1
            // PAID = 2
            // CANCELED = -1
            $table->enum('status', range(-10, 10))->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->uuid('plan_id')->index();
            $table->foreign('plan_id')->references('id')->on('plans');
            $table->uuid('recipient_id')->index();
            $table->foreign('recipient_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commissions');
    }
}
