<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoovMtnTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moov_mtn_transactions', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->timestamps();
            $table->softDeletes();
            $table->string('user_ref');
            $table->string('pricing_pk');
            $table->string('transaction_key');
            // DONE = 1
            // FAILED = -1
            // PENDING = 0
            $table->enum('status', range(-10, 10))->default(1);
            $table->enum('type', ['MOOV', 'MTN']);

            $table->json('request_body')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moov_mtn_transactions');
    }
}
