<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyStatusTypeToPerfectMoneyTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('perfect_money_transactions', function (Blueprint $table) {
            $table->dropColumn('status');
        });

        Schema::table('perfect_money_transactions', function (Blueprint $table) {
            // DONE = 1
            // FAILED = -1
            $table->enum('status', range(-10, 10))->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('perfect_money_transactions', function (Blueprint $table) {
            //
        });
    }
}
