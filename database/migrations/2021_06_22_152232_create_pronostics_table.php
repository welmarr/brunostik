<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePronosticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pronostics', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->time('hours');
            $table->integer('status')->nullable(true);
            $table->string('result')->nullable(true);
            $table->string('prediction');

            $table->uuid('team_one_id');
            $table->uuid('team_two_id');
            $table->uuid('sport_id');
            $table->uuid('bet_id');
            $table->uuid('championat_id');
            $table->uuid('event_id');
            $table->softDeletes();
            $table->timestamps();


            $table->foreign('team_one_id')->references('id')->on('teams');
            $table->foreign('team_two_id')->references('id')->on('teams');
            $table->foreign('sport_id')->references('id')->on('sports');
            $table->foreign('bet_id')->references('id')->on('bets');
            $table->foreign('championat_id')->references('id')->on('championats');
            $table->foreign('event_id')->references('id')->on('events');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pronostics');
    }
}
