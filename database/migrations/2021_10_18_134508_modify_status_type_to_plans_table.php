<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyStatusTypeToPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('plans', function (Blueprint $table) {
            $table->dropColumn('status');
        });
        Schema::table('plans', function (Blueprint $table) {
            // DONE = 1
            // END = 0
            // NOT_END_BUT_NEW_PLAN_DONE = -1 (overloaded_by is not null if status == -1)
            $table->enum('status', range(-10, 10))->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plans', function (Blueprint $table) {
            //
        });
    }
}
