<?php

namespace Database\Factories;

use App\Models\Pronostic;
use Illuminate\Database\Eloquent\Factories\Factory;

class PronosticFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Pronostic::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
