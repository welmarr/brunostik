<?php

namespace Database\Factories;

use App\Models\Championat;
use Illuminate\Database\Eloquent\Factories\Factory;

class ChampionatFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Championat::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
