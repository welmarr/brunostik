<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (User::where('email', 'root@greenh.com')->orWhere('username', 'rootlog')->count() == 0) {
            User::create([
                'forname' => 'Brunel',
                'surname' => 'ROOT',
                'email' => 'root@greenh.com',
                'username' => 'rootlog',
                'phone' => '000000000',
                'password' => bcrypt('12345'),
                'ref' => 'G007',
                'ref_perfectmoney' => '123456789ar',
                'token_id' => Uuid::uuid4()->toString() . time(),
            ]);
        }
    }
}
