<?php

namespace Database\Seeders;

use App\Models\Championat;
use Illuminate\Database\Seeder;

class ChampionatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 100; $i++) {
            Championat::create(['label' => "Championat#" . $i . '-' . time()]);
        }
    }
}