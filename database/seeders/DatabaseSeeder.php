<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{

    protected $toTruncate = ['users', 'pronostics', 'teams', 'sports', 'championats', 'events', 'bets'];

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /*Model::unguard();

        foreach ($this->toTruncate as $table) {
            DB::table($table)->truncate();
        }
        $this->call(UserSeeder::class);
        $this->call(TeamSeeder::class);
        $this->call(SportSeeder::class);
        $this->call(ChampionatSeeder::class);
        $this->call(EventSeeder::class);
        $this->call(BetSeeder::class);
        $this->call(PronosticSeeder::class);

        Model::reguard();*/

        $this->call(UserSeeder::class);
        $this->call(TeamSeeder::class);
        $this->call(SportSeeder::class);
        $this->call(ChampionatSeeder::class);
        $this->call(EventSeeder::class);
        $this->call(BetSeeder::class);
        $this->call(PronosticSeeder::class);
    }
}
