<?php

namespace Database\Seeders;

use App\Models\Team;
use Illuminate\Database\Seeder;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 100; $i++) {
            Team::create(['label' => (rand(0, 1) == 0 ? 'Equipe#' : 'Team#') . $i . '-' . time()]);
        }
    }
}