<?php

namespace Database\Seeders;

use App\Models\Bet;
use App\Models\Championat;
use App\Models\Event;
use App\Models\Pronostic;
use App\Models\Sport;
use App\Models\Team;
use Illuminate\Database\Seeder;

class PronosticSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bets = Bet::all();
        foreach ($bets as $key => $bet) {
            for ($i = 0; $i < rand(2, 4); $i++) {
                $ts = Team::inRandomOrder()->limit(2)->get();
                Pronostic::create([
                    'cote' => rand_float(1, 1000),
                    'status' => [1, 0, -1, null][rand(0, 3)],
                    'hours' => rand(1, 23) . ':' . rand(10, 59),
                    'result' => ['Sucess', 'Failed'][rand(0, 1)],
                    'prediction' => rand(0, 50) . ':' . rand(0, 59),
                    'team_one_id' => $ts->first()->id,
                    'team_two_id' => $ts->last()->id,
                    'sport_id' => Sport::inRandomOrder()->limit(2)->first()->id,
                    'bet_id' => $bet->id,
                    'championat_id' => Championat::inRandomOrder()->limit(2)->first()->id,
                    'event_id' => Event::inRandomOrder()->limit(2)->first()->id,
                ]);
            }
        }
    }
}
