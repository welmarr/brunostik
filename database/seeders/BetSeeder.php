<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Bet;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class BetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 20; $i++) {
            $for = Carbon::today()->subDays(rand(0, 365));
            for ($j = 0; $j < rand(0, 5); $j++) {
                $base = $i . $j . '-' . time();
                Bet::create(['label' => 'Label - ' . $base, 'code' => Str::random(rand(1, 4)) . $base, 'cote' => rand_float(1, 1000), 'status' => [1, 0, -1, null][rand(0, 3)], 'for' => $for]);
            }
        }
    }
}
