<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg"></a></p>

<h1 align="center">
    BRUNOSTIK Backend
</h1>

## <strong>Règles</strong>

## Request

-   The <strong>recorded_to</strong> and <strong>recorded_from</strong> query parameters used to filter the lists must be in the format `YYYY-MM-DD` (example: `2021-06-29`)

## Table public structure

-   ### **<u>Team</u>** (`api`: `/api/admin/teams`)

    -   ref: `uuid`
    -   designation: `string`
        -   **_sort_** (example: `?sort_by=-designation` or `?sort_by=designation`)
        -   **_filter_** (example: `?designation=Real`)
        -   **_create team_**
        -   **_update team_**
    -   recorded_at: `datetime`
        -   **_sort_** (example: `?sort_by=-recorded_at` or `?sort_by=recorded_at`)
        -   **_filter_** (example: `?recorded_from=2021-06-25&recorded_to=2021-06-29` or `?recorded_from=2021-06-25` or `?recorded_to=2021-06-29`)
    -   modified_at: `datetime`
    -   removed_at: `datetime` <br/><br/>

-   ### **<u>Sport</u>** (`api`: `/api/admin/sports`)

    -   ref: `uuid`
    -   designation: `string`
        -   **_sort_** (example: `?sort_by=-designation` or `?sort_by=designation`)
        -   **_filter_** (example: `?designation=Basket`)
        -   **_create sport_**
        -   **_update sport_**
    -   recorded_at: `datetime`
        -   **_sort_** (example: `?sort_by=-recorded_at` or `?sort_by=recorded_at`)
        -   **_filter_** (example: `?recorded_from=2021-06-25&recorded_to=2021-06-29` or `?recorded_from=2021-06-25` or `?recorded_to=2021-06-29`)
    -   modified_at: `datetime`
    -   removed_at: `datetime` <br/><br/>

-   ### **<u>Championat</u>** (`api`: `/api/admin/championats`)

    -   ref: `uuid`
    -   designation: `string`
        -   **_sort_** (example: `?sort_by=-designation` or `?sort_by=designation`)
        -   **_filter_** (example: `?designation=Europa`)
        -   **_create championat_**
        -   **_update championat_**
    -   recorded_at: `datetime`
        -   **_sort_** (example: `?sort_by=-recorded_at` or `?sort_by=recorded_at`)
        -   **_filter_** (example: `?recorded_from=2021-06-25&recorded_to=2021-06-29` or `?recorded_from=2021-06-25` or `?recorded_to=2021-06-29`)
    -   modified_at: `datetime`
    -   removed_at: `datetime`

-   ### **<u>Event</u>** (`api`: `/api/admin/events`)

    -   ref: `uuid`
    -   acronym: `string`
        -   **_sort_** (example: `?sort_by=-acronym` or `?sort_by=acronym`)
        -   **_filter_** (example: `?acronym=COR`)
        -   **_create event_** (**optional**)
        -   **_update event_** (**optional**)
    -   designation: `string`
        -   **_sort_** (example: `?sort_by=-designation` or `?sort_by=designation`)
        -   **_filter_** (example: `?designation=Corner`)
        -   **_create event_**
        -   **_update event_**
    -   recorded_at: `datetime`
        -   **_sort_** (example: `?sort_by=-recorded_at` or `?sort_by=recorded_at`)
        -   **_filter_** (example: `?recorded_from=2021-06-25&recorded_to=2021-06-29` or `?recorded_from=2021-06-25` or `?recorded_to=2021-06-29`)
    -   modified_at: `datetime`
    -   removed_at: `datetime` <br/><br/>

-   ### **<u>Bet</u>** 

    -   ref: `uuid`
    -   acronym: `string`
    -   cote: `string`
        -   **_sort_** (example: `?sort_by=-cote` or `?sort_by=cote`)
        -   **_filter_** (example: `?cote=2.6`)
        -   **_create event_**
        -   **_update event_** (**optional**)
    -   key (it's code): `string`
        -   **_sort_** (example: `?sort_by=-key` or `?sort_by=key`)
        -   **_filter_** (example: `?key=HYS8`)
        -   **_create event_**
        -   **_update event_** (**optional**)
    -   label: `string`
        -   **_sort_** (example: `?sort_by=-label` or `?sort_by=label`)
        -   **_filter_** (example: `?label=SoftPronostic`)
        -   **_create event_**
        -   **_update event_**
    -   status: `integer`
        -   **_sort_** (example: `?sort_by=-status` or `?sort_by=status`)
        -   **_filter_** (example: `?status=1`)
        -   **_update event_** (**optional**)
    -   available_at: `date`
        -   **_sort_** (example: `?sort_by=-available_at` or `?sort_by=available_at`)
        -   **_filter_** (example: `?available_from=2021-06-25&available_to=2021-06-29` or `?available_from=2021-06-25` or `?available_to=2021-06-29`)
        -   **_update event_** (**optional**)
    -   recorded_at: `datetime`
        -   **_sort_** (example: `?sort_by=-recorded_at` or `?sort_by=recorded_at`)
        -   **_filter_** (example: `?recorded_from=2021-06-25&recorded_to=2021-06-29` or `?recorded_from=2021-06-25` or `?recorded_to=2021-06-29`)
    -   modified_at: `datetime`
    -   removed_at: `datetime`


    1- **ROUTE ADMIN**  
`GET: api/admin/bets | POST: api/admin/bets | GET: api/admin/bets/{ref} | PUT: api/admin/bets/{ref}} | DELETE: api/admin/bets/{ref}`  
    2- **ROUTE OPEN**  
`GET: api/open/bets | GET: api/open/bets/{available_at}`
 <br/><br/>
-   ### **<u>Pronostic</u>** (`api`: `/api/admin/pronostics`)

    -   ref: `uuid`
    -   result: `string`
        -   **_sort_** (example: `?sort_by=-result` or `?sort_by=result`)
        -   **_filter_** (example: `?result=result`)
        -   **_update event_** (**optional**)
    -   status: `integer`
        -   **_sort_** (example: `?sort_by=-status` or `?sort_by=status`)
        -   **_filter_** (example: `?status=1`)
        -   **_update event_** (**optional**)
    -   hours: `string (H:i || example : 10:50)`
        -   **_sort_** (example: `?sort_by=-hours` or `?sort_by=hours`)
        -   **_filter_** (example: `?hours=10:50`)
        -   **_create event_**
        -   **_update event_** (**optional**)
    -   prediction: `string`
        -   **_sort_** (example: `?sort_by=-prediction` or `?sort_by=prediction`)
        -   **_filter_** (example: `?prediction=prediction`)
        -   **_create event_**
        -   **_update event_** (**optional**)
    -   cote: `string`
        -   **_sort_** (example: `?sort_by=-cote` or `?sort_by=cote`)
        -   **_filter_** (example: `?cote=cote`)
        -   **_create event_**
        -   **_update event_** (**optional**)
    -   team_one: `uuid (from team resource [ref])`
        -   **_sort_** (example: `?sort_by=-cote` or `?sort_by=cote`)
        -   **_filter_** (example: `?cote=cote`)
        -   **_create event_**
        -   **_update event_** (**optional**)
    -   team_two: `uuid (from team resource [ref])`
        -   **_sort_** (example: `?sort_by=-cote` or `?sort_by=cote`)
        -   **_filter_** (example: `?cote=cote`)
        -   **_create event_**
        -   **_update event_** (**optional**)
    -   sport: `uuid (from sport resource [ref])`
        -   **_sort_** (example: `?sort_by=-team_two` or `?sort_by=team_two`)
        -   **_create event_**
        -   **_update event_** (**optional**)
    -   bet: `uuid (from bet resource [ref])`
        -   **_sort_** (example: `?sort_by=-bet` or `?sort_by=bet`)
        -   **_create event_**
        -   **_update event_** (**optional**)
    -   championat: `uuid (from championat resource [ref])`
        -   **_sort_** (example: `?sort_by=-championat` or `?sort_by=championat`)
        -   **_create event_**
        -   **_update event_** (**optional**)
    -   event: `uuid (from event resource [ref])`
        -   **_sort_** (example: `?sort_by=-event` or `?sort_by=event`)
        -   **_create event_**
        -   **_update event_** (**optional**)
    -   recorded_at: `datetime`
        -   **_sort_** (example: `?sort_by=-recorded_at` or `?sort_by=recorded_at`)
        -   **_filter_** (example: `?recorded_from=2021-06-25&recorded_to=2021-06-29` or `?recorded_from=2021-06-25` or `?recorded_to=2021-06-29`)
    -   modified_at: `datetime`
    -   removed_at: `datetime` <br/><br/>
