<?php

namespace App\Interfaces\Opened;

use Illuminate\Http\Request;
use App\Http\Requests\MoovMtnTransactionCreateRequest;

interface MoovMtnTransactionRepositoryInterface
{
    /**
     * Get all moovmtntransactions
     *
     * @access  public
     */
    public function getAll(Request $request);

    /**
     * Create moovmtntransaction
     *
     * @param   \App\Http\Requests\MoovMtnTransactionCreateRequest    $request
     *
     * @access  public
     */
    public function create(MoovMtnTransactionCreateRequest $request);
}
