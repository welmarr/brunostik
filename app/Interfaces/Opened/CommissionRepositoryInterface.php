<?php

namespace App\Interfaces\Opened;

use Illuminate\Http\Request;
use App\Http\Requests\CommissionCreateOrUpdateRequest;

interface CommissionRepositoryInterface
{
    /**
     * Get all commissions associate to user connected
     *
     * @param \Illuminate\Http\Request $request
     * @access  public
     */
    public function getAll(Request $request);


    /**
     * Get Commission By ID
     *
     * @param   \Ramsey\Uuid\Uuid      $id
     *
     * @access  public
     */
    public function getById($id);
}
