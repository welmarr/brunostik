<?php

namespace App\Interfaces\Opened;

use Illuminate\Http\Request;
use App\Http\Requests\PlanCreateOrUpdateRequest;

interface PlanRepositoryInterface
{
    /**
     * Get all plans associate to user connected
     *
     * @param \Illuminate\Http\Request $request
     * @access  public
     */
    public function getAll(Request $request);


    /**
     * Get Plan By ID
     *
     * @param   \Ramsey\Uuid\Uuid      $id
     *
     * @access  public
     */
    public function getById($id);


    /**
     * Get Current Plan
     *
     *
     * @access  public
     */
    public function current();
}
