<?php

namespace App\Interfaces\Opened;

use Illuminate\Http\Request;
use App\Http\Requests\UserCreateOrUpdateRequest;

interface UserRepositoryInterface
{
    /**
     * Get all users associate to user cuonnected
     *
     * @access  public
     */
    public function getAllChildren(Request $request);
}
