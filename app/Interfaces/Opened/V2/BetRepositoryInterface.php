<?php

namespace App\Interfaces\Opened\V2;

use Illuminate\Http\Request;
use App\Http\Requests\BetCreateOrUpdateRequest;

interface BetRepositoryInterface
{
    /**
     * Get Bet By Date
     *
     * @param  \Illuminate\Http\Request    $request
     * @param   Date     $for
     *
     * @method  GET api/bets/{for}
     * @access  public
     */
    public function getByFor(Request $request, $for);
}
