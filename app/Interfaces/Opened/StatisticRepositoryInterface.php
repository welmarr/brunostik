<?php

namespace App\Interfaces\Opened;

use Illuminate\Http\Request;
use App\Http\Requests\StatisticCreateOrUpdateRequest;

interface StatisticRepositoryInterface
{
    /**
     *
     * @access  public
     */
    public function getGeneral();
}
