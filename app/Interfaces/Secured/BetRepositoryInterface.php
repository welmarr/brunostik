<?php

namespace App\Interfaces\Secured;

use Illuminate\Http\Request;
use App\Http\Requests\BetCreateOrUpdateRequest;

interface BetRepositoryInterface
{
    /**
     * Get all bets
     *
     * @param \Illuminate\Http\Request $request
     * @access  public
     */
    public function getAll(Request $request);

    /**
     * Get Bet By ID
     *
     * @param   \Ramsey\Uuid\Uuid      $id
     *
     * @access  public
     */
    public function getById($id);

    /**
     * Create | Update bet
     *
     * @param   \App\Http\Requests\BetCreateOrUpdateRequest    $request
     * @param   \Ramsey\Uuid\Uuid                           $id
     *
     * @access  public
     */
    public function createOrUpdate(BetCreateOrUpdateRequest $request, $id = null);

    /**
     * Delete bet
     *
     * @param   \Ramsey\Uuid\Uuid     $id
     *
     * @access  public
     */
    public function delete($id);
}
