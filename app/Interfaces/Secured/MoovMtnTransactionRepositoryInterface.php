<?php

namespace App\Interfaces\Secured;

use Illuminate\Http\Request;
use App\Http\Requests\MoovMtnTransactionManyUpdateRequest;

interface MoovMtnTransactionRepositoryInterface
{
    /**
     * Get all moovmtntransactions
     *
     * @access  public
     */
    public function getAll(Request $request);

    /**
     * Get MoovMtnTransaction By ID
     *
     * @param   \Ramsey\Uuid\Uuid      $id
     *
     * @access  public
     */
    public function getById($id);

    /**
     * Create | Update moovmtntransaction
     *
     * @param   \App\Http\Requests\MoovMtnTransactionManyUpdateRequest    $request
     *
     * @access  public
     */
    public function manyUpdate(MoovMtnTransactionManyUpdateRequest $request);

    /**
     * Delete moovmtntransaction
     *
     * @param   \Ramsey\Uuid\Uuid     $id
     *
     * @access  public
     */
    public function delete($id);
}
