<?php

namespace App\Interfaces\Secured;

use Illuminate\Http\Request;
use App\Http\Requests\PronosticCreateOrUpdateRequest;

interface PronosticRepositoryInterface
{
    /**
     * Get all Pronostics
     *
     * @access  public
     */
    public function getAll(Request $request);

    /**
     * Get Pronostic By ID
     *
     * @param   \Ramsey\Uuid\Uuid      $id
     *
     * @access  public
     */
    public function getById($id);

    /**
     * Create | Update Pronostic
     *
     * @param   \App\Http\Requests\PronosticCreateOrUpdateRequest    $request
     * @param   \Ramsey\Uuid\Uuid                           $id
     *
     * @access  public
     */
    public function createOrUpdate(PronosticCreateOrUpdateRequest $request, $id = null);

    /**
     * Delete Pronostic
     *
     * @param   \Ramsey\Uuid\Uuid     $id
     *
     * @access  public
     */
    public function delete($id);
}
