<?php

namespace App\Interfaces\Secured;

use Illuminate\Http\Request;
use App\Http\Requests\TeamCreateOrUpdateRequest;

interface TeamRepositoryInterface
{
    /**
     * Get all teams
     *
     * @method  GET api/teams
     * @access  public
     */
    public function getAll(Request $request);

    /**
     * Get Team By ID
     *
     * @param   \Ramsey\Uuid\Uuid      $id
     *
     * @method  GET api/teams/{id}
     * @access  public
     */
    public function getById($id);

    /**
     * Create | Update team
     *
     * @param   \App\Http\Requests\TeamCreateOrUpdateRequest    $request
     * @param   \Ramsey\Uuid\Uuid                           $id
     *
     * @method  POST    api/teams       For Create
     * @method  PUT     api/teams/{id}  For Update
     * @access  public
     */
    public function createOrUpdate(TeamCreateOrUpdateRequest $request, $id = null);

    /**
     * Delete team
     *
     * @param   \Ramsey\Uuid\Uuid     $id
     *
     * @method  DELETE  api/teams/{id}
     * @access  public
     */
    public function delete($id);
}
