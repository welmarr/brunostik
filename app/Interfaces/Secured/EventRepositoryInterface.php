<?php

namespace App\Interfaces\Secured;

use Illuminate\Http\Request;
use App\Http\Requests\EventCreateOrUpdateRequest;

interface EventRepositoryInterface
{
    /**
     * Get all events
     *
     * @method  GET api/events
     * @access  public
     */
    public function getAll(Request $request);

    /**
     * Get Event By ID
     *
     * @param   \Ramsey\Uuid\Uuid      $id
     *
     * @access  public
     */
    public function getById($id);

    /**
     * Create | Update event
     *
     * @param   \App\Http\Requests\EventCreateOrUpdateRequest    $request
     * @param   \Ramsey\Uuid\Uuid                           $id
     *
     * @access  public
     */
    public function createOrUpdate(EventCreateOrUpdateRequest $request, $id = null);

    /**
     * Delete event
     *
     * @param   \Ramsey\Uuid\Uuid     $id
     *
     * @access  public
     */
    public function delete($id);
}
