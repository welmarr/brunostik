<?php

namespace App\Interfaces\Secured\V2;

use Illuminate\Http\Request;
use App\Http\Requests\BetCreateOrUpdateRequest;

interface BetRepositoryInterface
{
    /**
     * Get all bets
     *
     * @param \Illuminate\Http\Request $request
     * @access  public
     */
    public function getAll(Request $request);

    /**
     * Get Bet By ID
     *
     * @param   \Ramsey\Uuid\Uuid      $id
     *
     * @access  public
     */
    public function getById($id);
}
