<?php

namespace App\Interfaces\Secured;

use Illuminate\Http\Request;
use App\Http\Requests\PricingCreateOrUpdateRequest;

interface PricingRepositoryInterface
{
    /**
     * Get all pricings
     *
     * @access  public
     */
    public function getAll(Request $request);

    /**
     * Get Pricing By ID
     *
     * @param   \Ramsey\Uuid\Uuid      $id
     *
     * @access  public
     */
    public function getById($id);

    /**
     * Create | Update pricing
     *
     * @param   \App\Http\Requests\PricingCreateOrUpdateRequest    $request
     * @param   \Ramsey\Uuid\Uuid                           $id
     *
     * @access  public
     */
    public function createOrUpdate(PricingCreateOrUpdateRequest $request, $id = null);

    /**
     * Delete pricing
     *
     * @param   \Ramsey\Uuid\Uuid     $id
     *
     * @access  public
     */
    public function delete($id);
}
