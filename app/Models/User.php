<?php

namespace App\Models;

use App\Traits\TokenId;
use App\Traits\Uuids;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasFactory, Notifiable, Uuids, HasApiTokens, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'forname', // Prénoms
        'surname', // Nom
        'email',
        'username',
        'phone',
        'password',
        'ref',
        'sponsor_ref_at_inscription',
        'sponsor_id',
        'token_id',
        'ref_perfectmoney',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sponsor()
    {
        return $this->belongsTo(User::class, 'sponsor_id');
    }

    public function children()
    {
        return $this->hasMany(User::class, 'sponsor_id');
    }

    public function commissions()
    {
        return $this->hasMany(Commission::class, 'recipient_id');
    }

    public function plans()
    {
        return $this->hasMany(Plan::class, 'user_id');
    }
}
