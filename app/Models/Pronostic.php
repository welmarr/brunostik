<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pronostic extends Model
{
    use HasFactory, Uuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'hours',
        'status',
        'cote',
        'result',
        'prediction',
        'team_one_id',
        'team_two_id',
        'sport_id',
        'bet_id',
        'championat_id',
        'event_id',
    ];


    public function teamOne()
    {
        return $this->belongsTo(Team::class, 'team_one_id');
    }

    public function teamTwo()
    {
        return $this->belongsTo(Team::class, 'team_two_id');
    }

    public function sport()
    {
        return $this->belongsTo(Sport::class, 'sport_id');
    }

    public function championat()
    {
        return $this->belongsTo(Championat::class, 'championat_id');
    }

    public function event()
    {
        return $this->belongsTo(Event::class, 'event_id');
    }

    public function bet()
    {
        return $this->belongsTo(Bet::class, 'bet_id');
    }
}
