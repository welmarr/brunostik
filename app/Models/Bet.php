<?php

namespace App\Models;

use App\Traits\Uuids;
use App\Models\V2\Pronostic as V2Pronostic;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Bet extends Model
{
    use HasFactory, Uuids, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'for',
        'code',
        'cote',
        'status',
        'label',
    ];


    public function pronostics()
    {
        return $this->hasMany(Pronostic::class, 'bet_id');
    }

    public function v2_pronostics()
    {
        return $this->hasMany(V2Pronostic::class, 'bet_id');
    }
}
