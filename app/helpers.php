<?php

function datetime_format_system()
{
    return 'Y-m-d H:i:s';
}

function date_format_system()
{
    return 'Y-m-d';
}

function datetime_format_db()
{
    return 'Y-m-d H:i:s';
}

function date_format_db()
{
    return 'Y-m-d';
}

function rand_float($st_num = 0, $end_num = 1, $mul = 1000000)
{
    if ($st_num > $end_num) return false;
    return mt_rand($st_num * $mul, $end_num * $mul) / $mul;
}