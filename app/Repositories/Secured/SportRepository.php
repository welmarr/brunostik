<?php

namespace App\Repositories\Secured;

use App\Models\Sport;
use App\Traits\Response;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\SportResource;
use App\Http\Resources\SportCollection;
use App\Interfaces\Secured\SportRepositoryInterface;
use App\Http\Requests\SportCreateOrUpdateRequest;

class SportRepository implements SportRepositoryInterface
{

    /**
     * Get all sports
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function getAll(Request $request)
    {
        try {
            $sports = null;

            if ($request->has('sort_by') && in_array($request->sort_by, ['designation', 'recorded_at', '-designation', '-recorded_at'])) {
                $sports = Sport::orderBy(['designation' => 'label', 'recorded_at' =>  'created_at', '-designation' => 'label', '-recorded_at' =>  'created_at'][$request->sort_by], $request->sort_by[0] == '-' ? 'asc' : 'desc');
            } else {
                $sports = Sport::orderBy('created_at', 'desc');
            }

            /*
             * Filtring on label sport table field
             * designation is the http request query key associated at label
             */
            if ($request->has('designation') && $request->designation) {
                $sports = Sport::where('label', 'LIKE', '%' . trim($request->designation) . '%');
            }
            /*
             * End filtring on label
             */

            /*
             * Filtring on created_at sport table field
             * created_at is datetime
             * recorded_from <= created_at
             * created_at <= recorded_to
             * recorded_at and recorded_from is date Y-m-d
             */

            if ($request->has('recorded_from') && $request->recorded_from) {
                $sports = $sports == null ? Sport::whereDate('created_at', '>=', $request->recorded_from) : $sports->whereDate('created_at', '>=', $request->recorded_from);
            }

            if ($request->has('recorded_to') && $request->recorded_to) {
                $sports = $sports == null ? Sport::whereDate('created_at', '<=', $request->recorded_to) : $sports->whereDate('created_at', '<=', $request->recorded_to);
            }
            /*
             * End filtring on created_at
             */

            $sports = $sports->paginate(25)->appends(Arr::except($request->query(), 'page'));
            return ['message' => "All Sports", 'data' => new SportCollection($sports), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }

    /**
     * Get Sport By ID
     *
     * @param \Ramsey\Uuid\Uuid  $id
     *
     * @return array
     */
    public function getById($id)
    {
        try {
            $sport = Sport::find($id);

            // Check the sport
            if (!$sport) return ['message' => "No sport with ID $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            return ['message' => "Sport Detail", 'data' => new SportResource($sport), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }

    /**
     * Create | Update sport
     *
     * @param \App\Http\Requests\SportCreateOrUpdateRequest $request
     * @param \Ramsey\Uuid\Uuid  $id
     *
     * @return array
     */
    public function createOrUpdate(SportCreateOrUpdateRequest $request, $id = null)
    {

        DB::beginTransaction();
        try {

            $sport = $id ? Sport::find($id) : new Sport;

            if ($id && !$sport) return ['message' => "No sport with ID $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            $sport->label = trim($request->designation);
            $sport->save();

            DB::commit();

            return ['message' => $id ? "Sport updated" : "Sport created", 'data' => $sport, 'statusCode' => $id ? 200 : 201];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }

    /**
     * Delete sport
     *
     * @param \Ramsey\Uuid\Uuid  $id
     *
     * @return array
     */
    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $sport = Sport::find($id);

            // Check the sport
            if (!$sport) return ['message' => "No sport with ID $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            $sport->label = $sport->label . "#-#DELETE#";
            $sport->save();

            // Delete the sport
            $sport->delete();

            DB::commit();
            return ['message' => "Sport deleted", 'data' => true, 'statusCode' => 200];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['message' => $e->getMessage(), 'data' => false, 'statusCode' => 500, 'error' => true];
        }
    }
}
