<?php

namespace App\Repositories\Secured;

use App\Models\Team;
use App\Traits\Response;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\TeamResource;
use App\Http\Resources\TeamCollection;
use Symfony\Component\Console\Input\Input;
use App\Interfaces\Secured\TeamRepositoryInterface;
use App\Http\Requests\TeamCreateOrUpdateRequest;

class TeamRepository implements TeamRepositoryInterface
{

    /**
     * Get all teams
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function getAll(Request $request)
    {
        try {
            $teams = null;

            if ($request->has('sort_by') && in_array($request->sort_by, ['designation', 'recorded_at', '-designation', '-recorded_at'])) {
                $teams = Team::orderBy(['designation' => 'label', 'recorded_at' =>  'created_at', '-designation' => 'label', '-recorded_at' =>  'created_at'][$request->sort_by], $request->sort_by[0] == '-' ? 'asc' : 'desc');
            } else {
                $teams = Team::orderBy('created_at', 'desc');
            }


            /*
             * Filtring on label team table field
             * designation is the http request query key associated at label
             */
            if ($request->has('designation') && $request->designation) {
                $teams = Team::where('label', 'LIKE', '%' . trim($request->designation) . '%');
            }
            /*
             * End filtring on label
             */


            /*
             * Filtring on created_at team table field
             * created_at is datetime
             * recorded_from <= created_at
             * created_at <= recorded_to
             * recorded_at and recorded_from is date Y-m-d
             */

            if ($request->has('recorded_from') && $request->recorded_from) {
                $teams = $teams == null ? Team::whereDate('created_at', '>=', $request->recorded_from) : $teams->whereDate('created_at', '>=', $request->recorded_from);
            }

            if ($request->has('recorded_to') && $request->recorded_to) {
                $teams = $teams == null ? Team::whereDate('created_at', '<=', $request->recorded_to) : $teams->whereDate('created_at', '<=', $request->recorded_to);
            }
            /*
             * End filtring on created_at
             */

            $teams = $teams->paginate(25)->appends(Arr::except($request->query(), 'page'));
            return ['message' => "All Teams", 'data' => new TeamCollection($teams), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }

    /**
     * Get Team By ID
     *
     * @param \Ramsey\Uuid\Uuid  $id
     *
     * @return array
     */
    public function getById($id)
    {
        try {
            $team = Team::find($id);

            // Check the team
            if (!$team) return ['message' => "No team with ID $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            return ['message' => "Team Detail", 'data' => new TeamResource($team), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }

    /**
     * Create | Update team
     *
     * @param \App\Http\Requests\TeamCreateOrUpdateRequest $request
     * @param \Ramsey\Uuid\Uuid  $id
     *
     * @return array
     */
    public function createOrUpdate(TeamCreateOrUpdateRequest $request, $id = null)
    {

        DB::beginTransaction();
        try {

            $team = $id ? Team::find($id) : new Team;

            if ($id && !$team) return ['message' => "No team with ID $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            $team->label = trim($request->designation);
            $team->save();

            DB::commit();

            return ['message' => $id ? "Team updated" : "Team created", 'data' => $team, 'statusCode' => $id ? 200 : 201];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }

    /**
     * Delete team
     *
     * @param \Ramsey\Uuid\Uuid  $id
     *
     * @return array
     */
    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $team = Team::find($id);

            // Check the team
            if (!$team) return ['message' => "No team with ID $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            $team->label = $team->label . "#-#DELETE#";
            $team->save();

            // Delete the team
            $team->delete();

            DB::commit();
            return ['message' => "Team deleted", 'data' => true, 'statusCode' => 200];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['message' => $e->getMessage(), 'data' => false, 'statusCode' => 500, 'error' => true];
        }
    }
}
