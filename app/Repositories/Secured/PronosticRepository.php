<?php

namespace App\Repositories\Secured;

use App\Models\Pronostic;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\PronosticResource;
use App\Http\Resources\PronosticCollection;
use App\Interfaces\Secured\PronosticRepositoryInterface;
use App\Http\Requests\PronosticCreateOrUpdateRequest;
use App\Http\Resources\PronosticCreateUpdateResource;

class PronosticRepository implements PronosticRepositoryInterface
{

    /**
     * Get all Pronostics
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function getAll(Request $request)
    {
        try {
            $pronostics = null;

            $sort_by_available_key = [
                'cote' => 'cote',
                'status' => 'status',
                'hours' => 'hours',
                'recorded_at' =>  'created_at',
                'prediction' => 'prediction',
                'team_one' => 'team_one_id',
                'team_two' => 'team_two_id',
                'sport' => 'sport_id',
                'bet' => 'bet_id',
                'championat' => 'championat_id',
                'event' => 'event_id',
                '-cote' => 'cote',
                '-status' => 'status',
                '-hours' => 'hours',
                '-recorded_at' =>  'created_at',
                '-prediction' => 'prediction',
                '-team_one' => 'team_one_id',
                '-team_two' => 'team_two_id',
                '-sport' => 'sport_id',
                '-bet' => 'bet_id',
                '-championat' => 'championat_id',
                '-event' => 'event_id',
            ];

            if ($request->has('sort_by') && in_array($request->sort_by, array_keys($sort_by_available_key))) {
                $pronostics = Pronostic::orderBy($sort_by_available_key[$request->sort_by], $request->sort_by[0] == '-' ? 'asc' : 'desc');
            } else {
                $pronostics = Pronostic::orderBy('created_at', 'desc');
            }


            /*
             * Filtring on result Pronostic table field
             * result is the http request query key associated at code
             */
            if ($request->has('result') && $request->result) {
                $pronostics = $pronostics->where('result', 'LIKE', '%' . trim($request->result) . '%');
            }
            /*
             * End filtring on result
             */


            /*
             * Filtring on prediction Pronostic table field
             * prediction is the http request query key associated at code
             */
            if ($request->has('prediction') && $request->prediction) {
                $pronostics = $pronostics->where('prediction', 'LIKE', '%' . trim($request->prediction) . '%');
            }
            /*
             * End filtring on prediction
             */

            /*
             * Filtring on cote Pronostic table field
             * cote is the http request query key associated at code
             */
            if ($request->has('cote') && $request->cote) {
                $pronostics = $pronostics->where('cote', 'LIKE', '%' . trim($request->cote) . '%');
            }
            /*
             * End filtring on cote
             */



            /*
             * Filtring on status Pronostic table field
             * status is the http request query key associated at status
             */

            //dd($request->status);
            if ($request->has('status') && in_array($request->status, [-1, 0, 1, 2])) {
                $pronostics = $request->status == 2 ? $pronostics->whereNull('status') : $pronostics->where('status', $request->status);
            }
            /*
             * End filtring on status
             */


            /*
             * Filtring on cote Pronostic table field
             * cote is date
             * cote_einf <= cote
             * cote <= cote_esup
             * cote_einf and cote_esup is float
             */

            if ($request->has('cote_einf') && $request->cote_einf) {
                $pronostics = $pronostics->where('cote', '>=', $request->cote_einf);
            }

            if ($request->has('cote_esup') && $request->cote_esup) {
                $pronostics = $pronostics->where('cote', '<=', $request->cote_esup);
            }
            /*
             * End filtring on cote
             */


            /*
             * Filtring on created_at Pronostic table field
             * created_at is datetime
             * recorded_from <= created_at
             * created_at <= recorded_to
             * recorded_at and recorded_from is date Y-m-d
             */

            if ($request->has('recorded_from') && $request->recorded_from) {
                $pronostics = $pronostics->whereDate('created_at', '>=', $request->recorded_from);
            }

            if ($request->has('recorded_to') && $request->recorded_to) {
                $pronostics = $pronostics->whereDate('created_at', '<=', $request->recorded_to);
            }
            /*
             * End filtring on created_at
             */

            $pronostics = $pronostics->paginate(25)->appends(Arr::except($request->query(), 'page'));
            return ['message' => "All Pronostics", 'data' => new PronosticCollection($pronostics), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }

    /**
     * Get Pronostic By ID
     *
     * @param \Ramsey\Uuid\Uuid  $id
     *
     * @return array
     */
    public function getById($id)
    {
        try {
            $pronostic = Pronostic::find($id);

            // Check the Pronostic
            if (!$pronostic) return ['message' => "No Pronostic with ID $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            return ['message' => "Pronostic Detail", 'data' => new PronosticResource($pronostic), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }

    /**
     * Create | Update Pronostic
     *
     * @param \App\Http\Requests\PronosticCreateOrUpdateRequest $request
     * @param \Ramsey\Uuid\Uuid  $id
     *
     * @return array
     */
    public function createOrUpdate(PronosticCreateOrUpdateRequest $request, $id = null)
    {

        DB::beginTransaction();
        try {

            $pronostic = $id ? Pronostic::find($id) : new Pronostic;

            if ($id && !$pronostic) return ['message' => "No Pronostic with ID $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            if ($id && $pronostic && $request->has('status')) {
                $pronostic->status = $request->status;
                $pronostic->save();
            }
            if ($id && $pronostic && $request->has('result')) {
                $pronostic->result = $request->result;
                $pronostic->save();
            }

            if ($request->has('hours')) {
                $pronostic->hours = $request->hours;
            }

            if ($request->has('cote')) {
                $pronostic->cote =  trim($request->cote);
            }

            if ($request->has('prediction')) {
                $pronostic->prediction = $request->prediction;
            }

            if ($request->has('team_one')) {
                $pronostic->team_one_id = $request->team_one;
            }

            if ($request->has('team_two')) {
                $pronostic->team_two_id = $request->team_two;
            }

            if ($request->has('sport')) {
                $pronostic->sport_id = $request->sport;
            }

            if ($request->has('bet')) {
                $pronostic->bet_id = $request->bet;
            }

            if ($request->has('championat')) {
                $pronostic->championat_id = $request->championat;
            }

            if ($request->has('event')) {
                $pronostic->event_id = $request->event;
            }

            $pronostic->save();

            DB::commit();

            return ['message' => $id ? "Pronostic updated" : "Pronostic created", 'data' => new PronosticCreateUpdateResource($pronostic), 'statusCode' => $id ? 200 : 201];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }

    /**
     * Delete Pronostic
     *
     * @param \Ramsey\Uuid\Uuid  $id
     *
     * @return array
     */
    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $pronostic = Pronostic::find($id);

            // Check the Pronostic
            if (!$pronostic) return ['message' => "No Pronostic with ID $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            $pronostic->cote = $pronostic->cote . "#-#DELETE#";
            $pronostic->save();

            // Delete the Pronostic
            $pronostic->delete();

            DB::commit();
            return ['message' => "Pronostic deleted", 'data' => true, 'statusCode' => 200];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['message' => $e->getMessage(), 'data' => false, 'statusCode' => 500, 'error' => true];
        }
    }
}
