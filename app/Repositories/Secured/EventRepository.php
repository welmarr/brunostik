<?php

namespace App\Repositories\Secured;

use App\Models\Event;
use App\Traits\Response;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\EventResource;
use App\Http\Resources\EventCollection;
use Symfony\Component\Console\Input\Input;
use App\Interfaces\Secured\EventRepositoryInterface;
use App\Http\Requests\EventCreateOrUpdateRequest;

class EventRepository implements EventRepositoryInterface
{

    /**
     * Get all events
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function getAll(Request $request)
    {
        try {
            $events = null;

            if ($request->has('sort_by') && in_array($request->sort_by, ['designation', 'recorded_at', 'acronym', '-designation', '-recorded_at', '-acronym'])) {
                $events = Event::orderBy(['designation' => 'label', 'recorded_at' =>  'created_at', 'acronym' => 'code', '-designation' => 'label', '-recorded_at' =>  'created_at', '-acronym' => 'code'][$request->sort_by], $request->sort_by[0] == '-' ? 'asc' : 'desc');
            } else {
                $events = Event::orderBy('created_at', 'desc');
            }

            /*
             * Filtring on label event table field
             * designation is the http request query key associated at label
             */
            if ($request->has('designation') && $request->designation) {
                $events = Event::where('label', 'LIKE', '%' . trim($request->designation) . '%');
            }
            /*
             * End filtring on label
             */



            /*
             * Filtring on code event table field
             * acronym is the http request query key associated at code
             */
            if ($request->has('acronym') && $request->acronym) {
                $events = Event::where('code', 'LIKE', '%' . trim($request->acronym) . '%');
            }
            /*
             * End filtring on code
             */


            /*
             * Filtring on created_at event table field
             * created_at is datetime
             * recorded_from <= created_at
             * created_at <= recorded_to
             * recorded_at and recorded_from is date Y-m-d
             */

            if ($request->has('recorded_from') && $request->recorded_from) {
                $events = $events == null ? Event::whereDate('created_at', '>=', $request->recorded_from) : $events->whereDate('created_at', '>=', $request->recorded_from);
            }

            if ($request->has('recorded_to') && $request->recorded_to) {
                $events = $events == null ? Event::whereDate('created_at', '<=', $request->recorded_to) : $events->whereDate('created_at', '<=', $request->recorded_to);
            }
            /*
             * End filtring on created_at
             */

            $events = $events->paginate(25)->appends(Arr::except($request->query(), 'page'));
            return ['message' => "All Events", 'data' => new EventCollection($events), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }

    /**
     * Get Event By ID
     *
     * @param \Ramsey\Uuid\Uuid  $id
     *
     * @return array
     */
    public function getById($id)
    {
        try {
            $event = Event::find($id);

            // Check the event
            if (!$event) return ['message' => "No event with ID $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            return ['message' => "Event Detail", 'data' => new EventResource($event), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }

    /**
     * Create | Update event
     *
     * @param \App\Http\Requests\EventCreateOrUpdateRequest $request
     * @param \Ramsey\Uuid\Uuid  $id
     *
     * @return array
     */
    public function createOrUpdate(EventCreateOrUpdateRequest $request, $id = null)
    {

        DB::beginTransaction();
        try {

            $event = $id ? Event::find($id) : new Event;

            if ($id && !$event) return ['message' => "No event with ID $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            $event->label = trim($request->label);

            if ($request->has('acronym') && $request->acronym) {
                $event->code = trim($request->acronym);
            }
            $event->save();

            DB::commit();

            return ['message' => $id ? "Event updated" : "Event created", 'data' => $event, 'statusCode' => $id ? 200 : 201];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }

    /**
     * Delete event
     *
     * @param \Ramsey\Uuid\Uuid  $id
     *
     * @return array
     */
    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $event = Event::find($id);

            // Check the event
            if (!$event) return ['message' => "No event with ID $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            $event->label = $event->label . "#-#DELETE#";
            $event->save();

            // Delete the event
            $event->delete();

            DB::commit();
            return ['message' => "Event deleted", 'data' => true, 'statusCode' => 200];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['message' => $e->getMessage(), 'data' => false, 'statusCode' => 500, 'error' => true];
        }
    }
}
