<?php

namespace App\Repositories\Secured;

use App\Models\Commission;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Http\Resources\CommissionResource;
use App\Http\Resources\CommissionCollection;
use App\Interfaces\Secured\CommissionRepositoryInterface;

class CommissionRepository implements CommissionRepositoryInterface
{

    /**
     * Get all commissions associate to user connected
     *
     * @param \Illuminate\Http\Request $request
     * @access  public
     */
    public function getAll(Request $request)
    {
        // PENDIND = 0
        // DONE = 1
        // CANCELED = -1

        try {
            $commissions = null;

            if ($request->filter && $request->filter == "d") {
                $commissions = Commission::where('status', 1)->paginate(25)->appends(Arr::except($request->query(), 'page'));
            } else if ($request->filter && $request->filter == "p") {
                $commissions = Commission::where('status', 0)->paginate(25)->appends(Arr::except($request->query(), 'page'));
            } else if ($request->filter && $request->filter == "f") {
                $commissions = Commission::where('status', -1)->paginate(25)->appends(Arr::except($request->query(), 'page'));
            } else {
                $commissions = Commission::paginate(25)->appends(Arr::except($request->query(), 'page'));
            }

            return ['message' => "All Commissions", 'data' => new CommissionCollection($commissions), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }


    /**
     * Get Commission By ID
     *
     * @param   \Ramsey\Uuid\Uuid      $id
     *
     * @access  public
     */
    public function getById($id)
    {
        try {
            $commission = Commission::where('id', $id)->first();

            // Check the commission
            if (!$commission) return ['message' => "No commission with ID $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            return ['message' => "Commission Detail", 'data' => new CommissionResource($commission), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }


    /**
     * Validated Commission By ID
     *
     * @param   \Ramsey\Uuid\Uuid      $id
     *
     * @access  public
     */
    public function validated($id)
    {
        try {
            $commission = Commission::where('id', $id)->first();

            // Check the commission
            if (!$commission) return ['message' => "No commission with ID $id", 'data' => null, 'statusCode' => 404, 'error' => true];
            $commission->status = 1;
            $commission->save();

            return ['message' => "Commission Detail", 'data' => new CommissionResource($commission), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }


    /**
     * Rejected Commission By ID
     *
     * @param   \Ramsey\Uuid\Uuid      $id
     *
     * @access  public
     */
    public function rejected($id)
    {
        try {
            $commission = Commission::where('id', $id)->first();

            // Check the commission
            if (!$commission) return ['message' => "No commission with ID $id", 'data' => null, 'statusCode' => 404, 'error' => true];
            $commission->status = -1;
            $commission->save();

            return ['message' => "Commission Detail", 'data' => new CommissionResource($commission), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }
}
