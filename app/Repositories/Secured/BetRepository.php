<?php

namespace App\Repositories\Secured;

use App\Models\Bet;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\BetResource;
use App\Http\Resources\BetCollection;
use App\Interfaces\Secured\BetRepositoryInterface;
use App\Http\Requests\BetCreateOrUpdateRequest;
use App\Http\Resources\BetCreateUpdateResource;

class BetRepository implements BetRepositoryInterface
{

    /**
     * Get all bets
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function getAll(Request $request)
    {
        try {
            $bets = null;

            $sort_by_available_key = [
                'key' => 'code',
                'cote' => 'cote',
                'status' => 'status',
                'label' => 'label',
                'recorded_at' =>  'created_at',
                'available_at' => 'for',
                '-key' => 'code',
                '-cote' => 'cote',
                '-status' => 'status',
                '-label' => 'label',
                '-recorded_at' =>  'created_at',
                '-available_at' => 'for'
            ];

            if ($request->has('sort_by') && in_array($request->sort_by, array_keys($sort_by_available_key))) {
                $bets = Bet::orderBy($sort_by_available_key[$request->sort_by], $request->sort_by[0] == '-' ? 'asc' : 'desc');
            } else {
                $bets = Bet::orderBy('created_at', 'desc');
            }


            /*
             * Filtring on code bet table field
             * key is the http request query key associated at code
             */
            if ($request->has('key') && $request->key) {
                $bets = $bets->where('code', 'LIKE', '%' . trim($request->key) . '%');
            }
            /*
             * End filtring on code
             */


            /*
             * Filtring on label bet table field
             * label is the http request query label associated at code
             */
            if ($request->has('label') && $request->label) {
                $bets = $bets->where('label', 'LIKE', '%' . trim($request->label) . '%');
            }
            /*
             * End filtring on label
             */



            /*
             * Filtring on status bet table field
             * status is the http request query key associated at status
             */

            //dd($request->status);
            if ($request->has('status') && in_array($request->status, [-1, 0, 1, 2])) {
                $bets = $request->status == 2 ? $bets->whereNull('status') : $bets->where('status', $request->status);
            }
            /*
             * End filtring on status
             */


            /*
             * Filtring on cote bet table field
             * cote is date
             * cote_einf <= cote
             * cote <= cote_esup
             * cote_einf and cote_esup is float
             */

            if ($request->has('cote_einf') && $request->cote_einf) {
                $bets = $bets->where('cote', '>=', $request->cote_einf);
            }

            if ($request->has('cote_esup') && $request->cote_esup) {
                $bets = $bets->where('cote', '<=', $request->cote_esup);
            }
            /*
             * End filtring on cote
             */


            /*
             * Filtring on for bet table field
             * for is date
             * available_from <= for
             * for <= available_to
             * available_at and available_from is date Y-m-d
             */

            if ($request->has('available_from') && $request->available_from) {
                $bets = $bets->where('for', '>=', $request->available_from);
            }

            if ($request->has('available_to') && $request->available_to) {
                $bets = $bets->where('for', '<=', $request->available_to);
            }
            /*
             * End filtring on for
             */


            /*
             * Filtring on created_at bet table field
             * created_at is datetime
             * recorded_from <= created_at
             * created_at <= recorded_to
             * recorded_at and recorded_from is date Y-m-d
             */

            if ($request->has('recorded_from') && $request->recorded_from) {
                $bets = $bets->whereDate('created_at', '>=', $request->recorded_from);
            }

            if ($request->has('recorded_to') && $request->recorded_to) {
                $bets = $bets->whereDate('created_at', '<=', $request->recorded_to);
            }
            /*
             * End filtring on created_at
             */

            $bets = $bets->paginate(25)->appends(Arr::except($request->query(), 'page'));
            return ['message' => "All Bets", 'data' => new BetCollection($bets), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }

    /**
     * Get Bet By ID
     *
     * @param \Ramsey\Uuid\Uuid  $id
     *
     * @return array
     */
    public function getById($id)
    {
        try {
            $bet = Bet::find($id);

            // Check the bet
            if (!$bet) return ['message' => "No bet with ID $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            return ['message' => "Bet Detail", 'data' => new BetResource($bet, true), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }

    /**
     * Create | Update bet
     *
     * @param \App\Http\Requests\BetCreateOrUpdateRequest $request
     * @param \Ramsey\Uuid\Uuid  $id
     *
     * @return array
     */
    public function createOrUpdate(BetCreateOrUpdateRequest $request, $id = null)
    {

        DB::beginTransaction();
        try {

            $bet = $id ? Bet::find($id) : new Bet;

            if ($id && !$bet) return ['message' => "No bet with ID $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            if ($id && $bet && $request->has('status')) {
                $bet->status = $request->status;
                $bet->save();
            }

            if ($request->has('key')) {
                $bet->code = trim($request->key);
            }
            if ($request->has('cote')) {
                $bet->cote = $request->cote;
            }
            if ($request->has('available_at')) {
                $bet->for = $request->available_at;
            }
            if ($request->has('label')) {
                $bet->label = $request->label;
            }

            $bet->save();


            DB::commit();

            return ['message' => $id ? "Bet updated" : "Bet created", 'data' => new BetCreateUpdateResource($bet), 'statusCode' => $id ? 200 : 201];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }

    /**
     * Delete bet
     *
     * @param \Ramsey\Uuid\Uuid  $id
     *
     * @return array
     */
    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $bet = Bet::find($id);

            // Check the bet
            if (!$bet) return ['message' => "No bet with ID $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            $bet->cote = $bet->cote . "#-#DELETE#";
            $bet->save();

            // Delete the bet
            $bet->delete();

            DB::commit();
            return ['message' => "Bet deleted", 'data' => true, 'statusCode' => 200];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['message' => $e->getMessage(), 'data' => false, 'statusCode' => 500, 'error' => true];
        }
    }
}
