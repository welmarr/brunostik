<?php

namespace App\Repositories\Opened;

use App\Traits\Response;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Models\MoovMtnTransaction;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\Input;
use App\Http\Resources\MoovMtnTransactionResource;
use App\Http\Resources\MoovMtnTransactionCollection;
use App\Http\Requests\MoovMtnTransactionCreateRequest;
use App\Interfaces\Opened\MoovMtnTransactionRepositoryInterface;

class MoovMtnTransactionRepository implements MoovMtnTransactionRepositoryInterface
{

    /**
     * Get all moovmtntransactions
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function getAll(Request $request)
    {
        try {
            $moovmtntransactions = null;

            $user = auth()->user();

            if ($request->has('sort_by') && in_array($request->sort_by, ['type', 'recorded_at', '-type', '-recorded_at'])) {
                $moovmtntransactions = MoovMtnTransaction::where('user_ref', $user->ref)->orderBy(['type' => 'type', 'recorded_at' =>  'created_at', '-type' => 'type', '-recorded_at' =>  'created_at'][$request->sort_by], $request->sort_by[0] == '-' ? 'asc' : 'desc');
            } else {
                $moovmtntransactions = MoovMtnTransaction::where('user_ref', $user->ref)->orderBy('created_at', 'desc');
            }


            /*
             * Filtring on type moovmtntransaction table field
             * type is the http request query key associated at type
             */
            if ($request->has('type') && $request->designation) {
                $moovmtntransactions = MoovMtnTransaction::where('type', 'LIKE', '%' . trim($request->designation) . '%');
            }
            /*
             * End filtring on type
             */


            /*
             * Filtring on created_at moovmtntransaction table field
             * created_at is datetime
             * recorded_from <= created_at
             * created_at <= recorded_to
             * recorded_at and recorded_from is date Y-m-d
             */

            if ($request->has('recorded_from') && $request->recorded_from) {
                $moovmtntransactions = $moovmtntransactions == null ? MoovMtnTransaction::whereDate('created_at', '>=', $request->recorded_from) : $moovmtntransactions->whereDate('created_at', '>=', $request->recorded_from);
            }

            if ($request->has('recorded_to') && $request->recorded_to) {
                $moovmtntransactions = $moovmtntransactions == null ? MoovMtnTransaction::whereDate('created_at', '<=', $request->recorded_to) : $moovmtntransactions->whereDate('created_at', '<=', $request->recorded_to);
            }
            /*
             * End filtring on created_at
             */

            $moovmtntransactions = $moovmtntransactions->paginate(25)->appends(Arr::except($request->query(), 'page'));
            return ['message' => "All MoovMtnTransactions", 'data' => new MoovMtnTransactionCollection($moovmtntransactions), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }

    /**
     * Create moovmtntransaction
     *
     * @param \App\Http\Requests\MoovMtnTransactionCreateRequest $request
     *
     * @return array
     */
    public function create(MoovMtnTransactionCreateRequest $request)
    {
        try {

            $moovmtntransaction = new MoovMtnTransaction;
            $moovmtntransaction->status = 0;
            $moovmtntransaction->transaction_key = $request->transaction;
            $moovmtntransaction->user_ref = auth()->user()->ref;
            $moovmtntransaction->pricing_pk = $request->princing;
            $moovmtntransaction->type = $request->type;
            $moovmtntransaction->save();



            return ['message' => "MoovMtnTransaction created", 'data' => new MoovMtnTransactionResource($moovmtntransaction), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => json_encode($e->getMessage()), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }
}
