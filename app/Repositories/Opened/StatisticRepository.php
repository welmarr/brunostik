<?php

namespace App\Repositories\Opened;

use App\Models\Commission;
use App\Interfaces\Opened\StatisticRepositoryInterface;
use App\Models\Plan;
use App\Models\User;

class StatisticRepository implements StatisticRepositoryInterface
{

    public function getGeneral()
    {
        try {
            $user = auth()->user();
            $commission = [
                'done' => Commission::where('recipient_id', $user->id)->where('status', 1)->sum('amount'),
                'pending' =>  Commission::where('recipient_id', $user->id)->where('status', 0)->sum('amount'),
                'cancel' => Commission::where('recipient_id', $user->id)->where('status', -1)->sum('amount'),
                'total' => Commission::where('recipient_id', $user->id)->sum('amount'),
            ];

            $substribtion = Plan::where('user_id', $user->id)->count();

            $downline = User::where('sponsor_id', $user->id)->count();

            return ['message' => "Statistic general", 'data' => compact("commission", "substribtion", "downline"), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }
}
