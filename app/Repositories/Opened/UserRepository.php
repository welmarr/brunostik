<?php

namespace App\Repositories\Opened;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Http\Resources\UserOpenCollection;
use App\Interfaces\Opened\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface
{

    /**
     * Get all users associate to user connected
     *
     * @param \Illuminate\Http\Request $request
     * @access  public
     */
    public function getAllChildren(Request $request)
    {
        try {
            $user = auth()->user();

            $children = $user->children()->paginate(25)->appends(Arr::except($request->query(), 'page'));
            return ['message' => "All Children", 'data' => new UserOpenCollection($children), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }

    /**
     * Get all users associate to user connected
     *
     * @access  public
     */
    public function subscription()
    {
        try {
            $user = auth()->user();

            $children = $user->children()->paginate(25)->appends(Arr::except($request->query(), 'page'));
            return ['message' => "All Children", 'data' => new UserOpenCollection($children), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }
}
