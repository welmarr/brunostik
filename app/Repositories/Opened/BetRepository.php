<?php

namespace App\Repositories\Opened;

use App\Models\Bet;
use App\Models\Plan;
use App\Models\Pricing;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Http\Resources\BetOnlyForCollection;
use App\Interfaces\Opened\BetRepositoryInterface;
use App\Http\Resources\BetWithPronosticCollection;
use Carbon\Carbon;

class BetRepository implements BetRepositoryInterface
{

    /**
     * Get all bets
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function getAll(Request $request)
    {
        try {
            $bets = null;

            /*$user = auth()->user();
            $plan = Plan::where('user_id', $user->id)->where('start', '<=', Carbon::now())->where('end', '>=', Carbon::now())->first();


            if (!$plan) {
                return ['message' => "All Bets", 'data' => [], 'statusCode' => 200];
            }*/

            $sort_by_available_key = [
                'status' => 'status',
                'available_at' => 'for',
                '-status' => 'status',
                '-available_at' => 'for'
            ];

            $bets = Bet::where('for', '<=', Carbon::now());

            if ($request->has('sort_by') && in_array($request->sort_by, array_keys($sort_by_available_key))) {
                $bets = $bets->orderBy($sort_by_available_key[$request->sort_by], $request->sort_by[0] == '-' ? 'asc' : 'desc');
            } else {
                $bets = $bets->orderBy('for', 'desc');
            }


            /*
             * Filtring on status bet table field
             * status is the http request query key associated at status
             */

            //dd($request->status);
            if ($request->has('status') && in_array($request->status, [-1, 0, 1, 2])) {
                $bets = $request->status == 2 ? $bets->whereNull('status') : $bets->where('status', $request->status);
            }
            /*
             * End filtring on status
             */


            /*
             * Filtring on for bet table field
             * for is date
             * available_from <= for
             * for <= available_to
             * available_at and available_from is date Y-m-d
             */

            if ($request->has('available_from') && $request->available_from) {
                $bets = $bets->where('for', '>=', $request->available_from);
            }

            if ($request->has('available_to') && $request->available_to) {
                $bets = $bets->where('for', '<=', $request->available_to);
            }
            /*
             * End filtring on for
             */

            $bets = $bets->distinct('for');

            $bets = $bets->paginate(25)->appends(Arr::except($request->query(), 'page'));
            return ['message' => "All Bets", 'data' => new BetOnlyForCollection($bets), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }

    /**
     * Get Bet By Date
     *
     * @param  \Illuminate\Http\Request    $request
     * @param   Date     $for
     *
     * @method  GET api/bets/{for}
     * @access  public
     */
    public function getByFor(Request $request, $for)
    {
        try {


            $user = auth()->user();
            $plan = Plan::where('user_id', $user->id)->where('start', '<=', Carbon::now())->where('end', '>=', Carbon::now())->first();


            $now = Carbon::now()->hour(0)->minute(0)->second(0);
            $cfor = Carbon::createFromFormat(date_format_db(), $for)->hour(23)->minute(59)->second(59);

            if ($now->lte($cfor) && !$plan) {
                return ['message' => "All Bets", 'data' => null, 'statusCode' => 402];
            }

            // $bets = Bet::where('for', $for)->where('for', '<=', $plan->end)->whereHas('pronostics')->orderBy('for', 'desc');
            $bets = Bet::where('for', $for)->whereHas('pronostics')->orderBy('for', 'desc');

            /*
             * Filtring on status bet table field
             * status is the http request query key associated at status
             */

            //dd($request->status);
            if ($request->has('status') && in_array($request->status, [-1, 0, 1, 2])) {
                $bets = $request->status == 2 ? $bets->whereNull('status') : $bets->where('status', $request->status);
            }
            /*
             * End filtring on status
             */

            $bets = $bets->paginate(25)->appends(Arr::except($request->query(), 'page'));
            return ['message' => "All Bets", 'data' => new BetWithPronosticCollection($bets), 'statusCode' => 200];
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }
}
