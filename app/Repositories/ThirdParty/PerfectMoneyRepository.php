<?php

namespace App\Repositories\ThirdParty;

use Carbon\Carbon;
use App\Models\Plan;
use App\Models\User;
use App\Models\PerfectMoneyTransaction;
use App\Http\Requests\PerfectMoneyFailedRequest;
use App\Http\Requests\PerfectMoneySuccessRequest;
use App\Interfaces\ThirdParty\PerfectMoneyRepositoryInterface;
use App\Models\Commission;
use App\Models\Pricing;

class PerfectMoneyRepository implements PerfectMoneyRepositoryInterface
{

    /**
     *
     * @param   \App\Http\Requests\PerfectMoneySuccessRequest    $request
     *
     * @access  public
     */
    public function success(PerfectMoneySuccessRequest $request)
    {
        //try {

        $transaction = PerfectMoneyTransaction::create([
            'user_ref' => trim($request->USER_REF),
            'pricing_pk' => $request->PLAN_PK,
            'request_body' => json_encode($request->all()),
        ]);

        $user = User::where('ref', $request->USER_REF)->first();
        $pricing = Pricing::where('public_key', $request->PLAN_PK)->first();


        if ($transaction && $user && $pricing) {

            $start = Carbon::now();
            $end = Carbon::now()->addDays($pricing->day_duration);

            $plan_old = PerfectMoneyTransaction::where('status', 1)->first();

            $plan = Plan::create([
                'user_id' => $user->id,
                'pricing_id' => $pricing->id,
                'transaction_key' => $request->PAYMENT_BATCH_NUM,
                'start' => $start,
                'end' => $end,
                'transaction_id' => $transaction->id,
                'transaction_class' => PerfectMoneyTransaction::class,
            ]);

            if ($user->sponsor) {
                $commission = Commission::create([
                    'amount' => (($pricing->gift * $pricing->price) / 100),
                    'plan_id' => $plan->id,
                    'recipient_id' => $user->sponsor->id,
                ]);
            }

            if ($plan_old) {
                $plan_old->status = 0;
                $plan_old->save();
            }
        } else {
            return $transaction ? 2 : -2;
        }

        return 1;
        /*} catch (\Exception $e) {
            return -1;
        }*/
    }

    /**
     *
     * @param   \App\Http\Requests\PerfectMoneySuccessRequest    $request
     *
     * @access  public
     */
    public function failed(PerfectMoneyFailedRequest $request)
    {
        try {
            PerfectMoneyTransaction::create([
                'user_ref' => $request->USER_REF,
                'pricing_pk' => $request->PLAN_PK,
                'status' => -1,
                'request_body' => json_encode($request->all()),
            ]);

            return 1;
        } catch (\Exception $e) {
            return -1;
        }
    }
}
