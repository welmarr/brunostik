<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Secured\BetRepository as BetRepositorySecured;
use App\Interfaces\Secured\BetRepositoryInterface as BetRepositoryInterfaceSecured;
use App\Repositories\Secured\TeamRepository as TeamRepositorySecured;
use App\Interfaces\Secured\TeamRepositoryInterface as TeamRepositoryInterfaceSecured;
use App\Repositories\Secured\PricingRepository as PricingRepositorySecured;
use App\Interfaces\Secured\PricingRepositoryInterface as PricingRepositoryInterfaceSecured;
use App\Repositories\Secured\EventRepository as EventRepositorySecured;
use App\Interfaces\Secured\EventRepositoryInterface as EventRepositoryInterfaceSecured;
use App\Repositories\Secured\SportRepository as SportRepositorySecured;
use App\Interfaces\Secured\SportRepositoryInterface as SportRepositoryInterfaceSecured;
use App\Repositories\Secured\PronosticRepository as PronosticRepositorySecured;
use App\Interfaces\Secured\PronosticRepositoryInterface as PronosticRepositoryInterfaceSecured;
use App\Repositories\Secured\ChampionatRepository as ChampionatRepositorySecured;
use App\Interfaces\Secured\ChampionatRepositoryInterface as ChampionatRepositoryInterfaceSecured;
use App\Repositories\Opened\BetRepository as BetRepositoryOpened;
use App\Interfaces\Opened\BetRepositoryInterface as BetRepositoryInterfaceOpened;
use App\Repositories\Opened\UserRepository as UserRepositoryOpened;
use App\Interfaces\Opened\UserRepositoryInterface as UserRepositoryInterfaceOpened;
use App\Repositories\Opened\CommissionRepository as CommissionRepositoryOpened;
use App\Interfaces\Opened\CommissionRepositoryInterface as CommissionRepositoryInterfaceOpened;
use App\Repositories\Opened\PlanRepository as PlanRepositoryOpened;
use App\Interfaces\Opened\PlanRepositoryInterface as PlanRepositoryInterfaceOpened;
use App\Repositories\Secured\CommissionRepository as CommissionRepositorySecured;
use App\Interfaces\Secured\CommissionRepositoryInterface as CommissionRepositoryInterfaceSecured;
use App\Repositories\Opened\StatisticRepository as StatisticRepositoryOpened;
use App\Interfaces\Opened\StatisticRepositoryInterface as StatisticRepositoryInterfaceOpened;

use App\Repositories\Opened\MoovMtnTransactionRepository as MoovMtnTransactionRepositoryOpened;
use App\Interfaces\Opened\MoovMtnTransactionRepositoryInterface as MoovMtnTransactionRepositoryInterfaceOpened;

use App\Repositories\Secured\MoovMtnTransactionRepository as MoovMtnTransactionRepositorySecured;
use App\Interfaces\Secured\MoovMtnTransactionRepositoryInterface as MoovMtnTransactionRepositoryInterfaceSecured;


use App\Repositories\Secured\V2\PronosticRepository as V2PronosticRepositorySecured;
use App\Interfaces\Secured\V2\PronosticRepositoryInterface as V2PronosticRepositoryInterfaceSecured;
use App\Repositories\Secured\V2\BetRepository as V2BetRepositorySecured;
use App\Interfaces\Secured\V2\BetRepositoryInterface as V2BetRepositoryInterfaceSecured;

use App\Repositories\Opened\V2\BetRepository as V2BetRepositoryOpened;
use App\Interfaces\Opened\V2\BetRepositoryInterface as V2BetRepositoryInterfaceOpened;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(TeamRepositoryInterfaceSecured::class, TeamRepositorySecured::class);
        $this->app->bind(SportRepositoryInterfaceSecured::class, SportRepositorySecured::class);
        $this->app->bind(ChampionatRepositoryInterfaceSecured::class, ChampionatRepositorySecured::class);
        $this->app->bind(EventRepositoryInterfaceSecured::class, EventRepositorySecured::class);
        $this->app->bind(BetRepositoryInterfaceSecured::class, BetRepositorySecured::class);
        $this->app->bind(PronosticRepositoryInterfaceSecured::class, PronosticRepositorySecured::class);
        $this->app->bind(PricingRepositoryInterfaceSecured::class, PricingRepositorySecured::class);
        $this->app->bind(CommissionRepositoryInterfaceSecured::class, CommissionRepositorySecured::class);
        $this->app->bind(MoovMtnTransactionRepositoryInterfaceSecured::class, MoovMtnTransactionRepositorySecured::class);

        $this->app->bind(BetRepositoryInterfaceOpened::class, BetRepositoryOpened::class);
        $this->app->bind(UserRepositoryInterfaceOpened::class, UserRepositoryOpened::class);
        $this->app->bind(CommissionRepositoryInterfaceOpened::class, CommissionRepositoryOpened::class);
        $this->app->bind(PlanRepositoryInterfaceOpened::class, PlanRepositoryOpened::class);
        $this->app->bind(StatisticRepositoryInterfaceOpened::class, StatisticRepositoryOpened::class);
        $this->app->bind(MoovMtnTransactionRepositoryInterfaceOpened::class, MoovMtnTransactionRepositoryOpened::class);

        $this->app->bind(V2PronosticRepositoryInterfaceSecured::class, V2PronosticRepositorySecured::class);
        $this->app->bind(V2BetRepositoryInterfaceSecured::class, V2BetRepositorySecured::class);
        $this->app->bind(V2PronosticRepositoryInterfaceSecured::class, V2PronosticRepositorySecured::class);
        $this->app->bind(V2BetRepositoryInterfaceOpened::class, V2BetRepositoryOpened::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
