<?php

namespace App\Http\Resources;

use App\Models\Pricing;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class MoovMtnTransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'ref' => $this->id,
            'status' => $this->status,
            'type' => $this->type,
            'transaction' => $this->transaction_key,
            'user' => new UserOpenResource(User::where('ref', $this->user_ref)->first()),
            'pricing' => new PricingResource(Pricing::where('public_key', $this->pricing_pk)->first()),
            'recorded_at' => $this->created_at ? Carbon::createFromFormat(datetime_format_db(), $this->created_at)->format(datetime_format_system()) : null,
            'modified_at' => $this->updated_at ? Carbon::createFromFormat(datetime_format_db(), $this->updated_at)->format(datetime_format_system()) : null,
            'removed_at' => $this->deleted_at ? Carbon::createFromFormat(datetime_format_db(), $this->deleted_at)->format(datetime_format_system()) : null,
        ];
    }
}
