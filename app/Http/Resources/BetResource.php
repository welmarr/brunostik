<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class BetResource extends JsonResource
{

    /**
     * @var
     */
    private $with_pronostics;

    /**
     * Create a new resource instance.
     *
     * @param  mixed  $resource
     * @param  bool  $with_pronostics
     * @return void
     */
    public function __construct($resource, $with_pronostics = false)
    {
        // Ensure you call the parent constructor
        parent::__construct($resource);
        $this->resource = $resource;

        $this->with_pronostics = $with_pronostics;
    }


    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        $out = [
            'ref' => $this->id,
            'key' => $this->code,
            'cote' => $this->cote,
            'label' => $this->label,
            'status' => $this->status,
            'available_at' => $this->for ? Carbon::createFromFormat(date_format_db(), $this->for)->format(date_format_system()) : null,
            'recorded_at' => $this->created_at ? Carbon::createFromFormat(datetime_format_db(), $this->created_at)->format(datetime_format_system()) : null,
            'modified_at' => $this->updated_at ? Carbon::createFromFormat(datetime_format_db(), $this->updated_at)->format(datetime_format_system()) : null,
            'removed_at' => $this->deleted_at ? Carbon::createFromFormat(datetime_format_db(), $this->deleted_at)->format(datetime_format_system()) : null,
            'href' => [
                'link' => route('admin.bets.show', ['bet' => $this->id]),
            ],
        ];
        if ($this->with_pronostics) {
            $pronostics = $this->pronostics->map(function ($pronostic) {
                return new PronosticResource($pronostic, false);
            });

            $out['pronostics'] = $pronostics->all();
        }
        return $out;
    }
}
