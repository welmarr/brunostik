<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class PronosticCreateUpdateResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $out = [
            'cote' => $this->cote,
            'ref' => $this->id,
            'status' => $this->status,
            'result' => $this->result,
            'hours' => $this->hours,
            'prediction' => $this->prediction,
            'team_one' => new TeamCreateUpdateResource($this->teamOne),
            'team_two' => new TeamCreateUpdateResource($this->teamTwo),
            'sport' => new SportCreateUpdateResource($this->sport),
            'championat' => new ChampionatCreateUpdateResource($this->championat),
            'event'  => new EventCreateUpdateResource($this->event),
        ];

        if ($this->with_bet) {
            $out['bet'] = new BetCreateUpdateResource($this->bet);
        }

        $out['recorded_at'] = $this->created_at ? Carbon::createFromFormat(datetime_format_db(), $this->created_at)->format(datetime_format_system()) : null;
        $out['modified_at'] = $this->updated_at ? Carbon::createFromFormat(datetime_format_db(), $this->updated_at)->format(datetime_format_system()) : null;
        $out['removed_at'] = $this->deleted_at ? Carbon::createFromFormat(datetime_format_db(), $this->deleted_at)->format(datetime_format_system()) : null;

        return $out;
    }
}
