<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class UserOpenResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        //dd($this->sponsor);
        return [
            'ref' => $this->id,
            'firstname' => $this->forname,
            'lastname' => $this->surname,
            'username' => $this->username,
            'email' => $this->email,
            'phone' => $this->phone,
            'personrefkey' => $this->ref,
        ];
    }
}
