<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class PronosticResource extends JsonResource
{

    /**
     * @var
     */
    private $with_bet;

    /**
     * Create a new resource instance.
     *
     * @param  mixed  $resource
     * @param  bool  $with_bet
     * @return void
     */
    public function __construct($resource, $with_bet = true)
    {
        // Ensure you call the parent constructor
        parent::__construct($resource);
        $this->resource = $resource;

        $this->with_bet = $with_bet;
    }


    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $out = [
            'cote' => $this->cote,
            'ref' => $this->id,
            'status' => $this->status,
            'result' => $this->result,
            'hours' => $this->hours,
            'prediction' => $this->prediction,
            'team_one' => new TeamResource($this->teamOne),
            'team_two' => new TeamResource($this->teamTwo),
            'sport' => new SportResource($this->sport),
            'championat' => new ChampionatResource($this->championat),
            'event'  => new EventResource($this->event),
        ];

        if ($this->with_bet) {
            $out['bet'] = new BetResource($this->bet);
        }

        $out['recorded_at'] = $this->created_at ? Carbon::createFromFormat(datetime_format_db(), $this->created_at)->format(datetime_format_system()) : null;
        $out['modified_at'] = $this->updated_at ? Carbon::createFromFormat(datetime_format_db(), $this->updated_at)->format(datetime_format_system()) : null;
        $out['removed_at'] = $this->deleted_at ? Carbon::createFromFormat(datetime_format_db(), $this->deleted_at)->format(datetime_format_system()) : null;
        $out['href'] = [
            'link' => route('admin.pronostics.show', ['pronostic' => $this->id]),
        ];

        return $out;
    }
}
