<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class UserLoginResource extends JsonResource
{

    /**
     * @var
     */
    private $token;

    /**
     * Create a new resource instance.
     *
     * @param  mixed  $resource
     * @param  bool  $token
     * @return void
     */
    public function __construct($resource, $token = false)
    {
        // Ensure you call the parent constructor
        parent::__construct($resource);
        $this->resource = $resource;

        $this->token = $token;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        //dd($this->sponsor);
        return [
            'ref' => $this->id,
            'firstname' => $this->forname,
            'lastname' => $this->surname,
            'username' => $this->username,
            'email' => $this->email,
            'phone' => $this->phone,
            'personrefkey' => $this->ref,
            'sponsref' => $this->sponsor ? ['firstname' => $this->sponsor->forname, 'lastname' => $this->sponsor->surname, 'personrefkey' => $this->sponsor->ref,] : null,
            'token' => $this->token,
        ];
    }
}
