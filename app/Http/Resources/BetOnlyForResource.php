<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class BetOnlyForResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $out = [
            'available_at' => $this->for ? Carbon::createFromFormat(date_format_db(), $this->for)->format(date_format_system()) : null,
            'size' => $this->for ? \App\Models\Bet::where('for', $this->for)->count() : 0,
        ];
        return $out;
    }
}
