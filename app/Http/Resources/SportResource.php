<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class SportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'ref' => $this->id,
            'designation' => $this->label,
            'recorded_at' => $this->created_at ? Carbon::createFromFormat(datetime_format_db(), $this->created_at)->format(datetime_format_system()) : null,
            'modified_at' => $this->updated_at ? Carbon::createFromFormat(datetime_format_db(), $this->updated_at)->format(datetime_format_system()) : null,
            'removed_at' => $this->deleted_at ? Carbon::createFromFormat(datetime_format_db(), $this->deleted_at)->format(datetime_format_system()) : null,
            'href' => [
                'link' => route('admin.sports.show', ['sport' => $this->id]),
            ],
        ];
    }
}
