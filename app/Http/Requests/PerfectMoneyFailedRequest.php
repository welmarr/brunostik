<?php

namespace App\Http\Requests;

use App\Traits\Response;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class PerfectMoneyFailedRequest extends FormRequest
{
    use Response;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'PAYEE_ACCOUNT' => 'required|min:1|max:250',
            'PAYMENT_AMOUNT' => 'required|min:1|max:250',
            'PAYMENT_UNITS' => 'required|min:1|max:250',
            'PAYMENT_BATCH_NUM' => 'required|integer|min:0|not_in:0',
            'USER_REF' => 'required|min:1|max:250',
            'PLAN_PK' => 'required|min:1|max:250',
        ];
    }



    protected function failedValidation(Validator $validator)
    {

        return redirect()->away('https://green-house-official.com/paiement/failed');
        /* throw new HttpResponseException(
            $this->response(['message' => $validator->errors(), 'error' => true, 'statusCode' => 400])
        ); */
    }
}
