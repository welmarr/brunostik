<?php

namespace App\Http\Requests;

use App\Traits\Response;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class PronosticCreateOrUpdateRequest extends FormRequest
{
    use Response;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->route('pronostic') && \Ramsey\Uuid\Uuid::isValid(request()->route('pronostic'))) {

            return [
                'status' => 'numeric|in:1,0,1',
                'result' => 'min:3|max:250',
                'hours'  => 'date_format:H:i',
                'prediction' => 'min:3|max:250',
                'team_one' => 'uuid|exists:teams,id',
                'team_two' => 'uuid|exists:teams,id',
                'sport' => 'uuid|exists:sports,id',
                'bet' => 'uuid|exists:bets,id',
                'championat' => 'uuid|exists:championats,id',
                'event' => 'uuid|exists:events,id',
                'cote' => 'min:3|max:250',
            ];
        }
        return [
            'hours'  => 'required|date_format:H:i',
            'prediction' => 'required|min:3|max:250',
            'team_one' => 'required|uuid|exists:teams,id',
            'team_two' => 'required|uuid|exists:teams,id',
            'sport' => 'required|uuid|exists:sports,id',
            'bet' => 'required|uuid|exists:bets,id',
            'championat' => 'required|uuid|exists:championats,id',
            'event' => 'required|uuid|exists:events,id',
            'cote' => 'required|min:3|max:250',
        ];
    }



    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            $this->response(['message' => $validator->errors(), 'error' => true, 'statusCode' => 400])
        );
    }
}
