<?php

namespace App\Http\Requests\V2;

use App\Traits\Response;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class PronosticCreateOrUpdateRequest extends FormRequest
{
    use Response;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->route('pronostic') && \Ramsey\Uuid\Uuid::isValid(request()->route('pronostic'))) {

            return [
                'status' => 'numeric|in:1,0,1',
                'result' => 'min:1|max:250',
                'hours'  => 'date_format:H:i',
                'prediction' => 'min:1|max:250',
                'team_one' => 'min:1|max:250',
                'team_two' => 'min:1|max:250',
                'sport' => 'min:1|max:250',
                'bet' => 'uuid|exists:bets,id',
                'championat' => 'min:1|max:250',
                'event' => 'min:1|max:250',
                'cote' => 'min:1|max:250',
            ];
        }
        return [
            'hours'  => 'required|date_format:H:i',
            'prediction' => 'required|min:1|max:250',
            'team_one' => 'required|min:1|max:250',
            'team_two' => 'required|min:1|max:250',
            'sport' => 'required|min:1|max:250',
            'bet' => 'required|uuid|exists:bets,id',
            'championat' => 'required|min:1|max:250',
            'event' => 'required|min:1|max:250',
            'cote' => 'required|min:1|max:250',
        ];
    }



    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            $this->response(['message' => $validator->errors(), 'error' => true, 'statusCode' => 400])
        );
    }
}
