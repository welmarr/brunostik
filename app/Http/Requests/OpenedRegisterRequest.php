<?php

namespace App\Http\Requests;

use App\Traits\Response;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class OpenedRegisterRequest extends FormRequest
{
    use Response;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required|min:2|max:200',
            'lastname' => 'required|min:2|max:200',
            'username' => 'required|min:2|max:200|unique:users,username',
            'email' => 'required|email|unique:users,email',
            'scode' => 'required|min:5|max:250',
            'phone' => 'required|unique:users,phone',
            'personrefkey' => 'required|unique:users,ref',
            'sponsrefkey' => 'required|exists:users,ref',
            'perfect' => 'nullable|unique:users,ref_perfectmoney,NULL',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        // dd($validator->errors(), request()->all());
        // throw new HttpResponseException(response()->json($validator->errors(), 422));
        throw new HttpResponseException(
            $this->response(['message' => $validator->errors(), 'error' => true, 'statusCode' => 400])
        );
    }
}
