<?php

namespace App\Http\Controllers\Secured;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\Secured\BetRepositoryInterface;
use App\Http\Requests\BetCreateOrUpdateRequest;

class BetController extends Controller
{
    protected $betRepository;

    public function __construct(BetRepositoryInterface $betRepository)
    {
        $this->betRepository = $betRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->response($this->betRepository->getAll($request));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BetCreateOrUpdateRequest $request)
    {
        return $this->response($this->betRepository->createOrUpdate($request));
    }

    /**
     * Display the specified resource.
     *
     * @param  \ramsey\uuid\uuid  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->response($this->betRepository->getById($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \ramsey\uuid\uuid  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BetCreateOrUpdateRequest $request, $id)
    {
        return $this->response($this->betRepository->createOrUpdate($request, $id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \ramsey\uuid\uuid  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->response($this->betRepository->delete($id));
    }
}
