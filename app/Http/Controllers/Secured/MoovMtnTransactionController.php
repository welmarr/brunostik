<?php

namespace App\Http\Controllers\Secured;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\Secured\MoovMtnTransactionRepositoryInterface;
use App\Http\Requests\MoovMtnTransactionCreateOrUpdateRequest;
use App\Http\Requests\MoovMtnTransactionManyUpdateRequest;

class MoovMtnTransactionController extends Controller
{
    protected $moovmtntransactionRepository;

    public function __construct(MoovMtnTransactionRepositoryInterface $moovmtntransactionRepository)
    {
        $this->moovmtntransactionRepository = $moovmtntransactionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->response($this->moovmtntransactionRepository->getAll($request));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MoovMtnTransactionManyUpdateRequest $request)
    {
        return $this->response($this->moovmtntransactionRepository->manyUpdate($request));
    }

    /**
     * Display the specified resource.
     *
     * @param  \ramsey\uuid\uuid  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->response($this->moovmtntransactionRepository->getById($id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \ramsey\uuid\uuid  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->response($this->moovmtntransactionRepository->delete($id));
    }
}
