<?php

namespace App\Http\Controllers\Secured;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\Secured\CommissionRepositoryInterface;
use App\Http\Requests\CommissionCreateOrUpdateRequest;

class CommissionController extends Controller
{
    protected $commissionRepository;

    public function __construct(CommissionRepositoryInterface $commissionRepository)
    {
        $this->commissionRepository = $commissionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->response($this->commissionRepository->getAll($request));
    }

    /**
     * Display the specified resource.
     *
     * @param  \ramsey\uuid\uuid  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->response($this->commissionRepository->getById($id));
    }

    /**
     * Accepte the specified commission.
     *
     * @param  \ramsey\uuid\uuid  $id
     * @return \Illuminate\Http\Response
     */
    public function validated($id)
    {
        return $this->response($this->commissionRepository->validated($id));
    }

    /**
     * Reject the specified commission.
     *
     * @param  \ramsey\uuid\uuid  $id
     * @return \Illuminate\Http\Response
     */
    public function rejected($id)
    {
        return $this->response($this->commissionRepository->rejected($id));
    }
}
