<?php

namespace App\Http\Controllers\Secured;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\Secured\ChampionatRepositoryInterface;
use App\Http\Requests\ChampionatCreateOrUpdateRequest;

class ChampionatController extends Controller
{
    protected $championatRepository;

    public function __construct(ChampionatRepositoryInterface $championatRepository)
    {
        $this->championatRepository = $championatRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->response($this->championatRepository->getAll($request));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ChampionatCreateOrUpdateRequest $request)
    {
        return $this->response($this->championatRepository->createOrUpdate($request));
    }

    /**
     * Display the specified resource.
     *
     * @param  \ramsey\uuid\uuid  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->response($this->championatRepository->getById($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \ramsey\uuid\uuid  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ChampionatCreateOrUpdateRequest $request, $id)
    {
        return $this->response($this->championatRepository->createOrUpdate($request, $id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \ramsey\uuid\uuid  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->response($this->championatRepository->delete($id));
    }
}
