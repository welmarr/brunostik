<?php

namespace App\Http\Controllers\Secured\V2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\Secured\V2\BetRepositoryInterface;

class BetController extends Controller
{
    protected $betRepository;

    public function __construct(BetRepositoryInterface $betRepository)
    {
        $this->betRepository = $betRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->response($this->betRepository->getAll($request));
    }

    /**
     * Display the specified resource.
     *
     * @param  \ramsey\uuid\uuid  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->response($this->betRepository->getById($id));
    }
}
