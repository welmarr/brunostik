<?php

namespace App\Http\Controllers\Opened;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\Opened\BetRepositoryInterface;

class BetController extends Controller
{
    protected $betRepository;

    public function __construct(BetRepositoryInterface $betRepository)
    {
        $this->betRepository = $betRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->response($this->betRepository->getAll($request));
    }
    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request    $request
     * @param  \Date  $for
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $for)
    {
        return $this->response($this->betRepository->getByFor($request, $for));
    }
}
