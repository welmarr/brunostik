<?php

namespace App\Http\Controllers\Opened\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\OpenedLoginRequest;
use App\Http\Resources\UserLoginResource;

class LoginController extends Controller
{
    static public $err_class_code = "LO2000";

    public function login(OpenedLoginRequest $request)
    {
        /*
         * ERROR CODE
         * LO2000-XS-FIC = FAILDED INVALID CREDENTIAL
         * LO2000-XS-ISECI = INTERNAL SERVER ERROR INVALID CREDENTIAL
         * LO2000-XS-ISE = INTERNAL SERVER ERROR
            */
            $err_function_code = self::$err_class_code . '-' . 'XS' . '-';


        //try {
        $users = User::where('email', strtolower(trim($request->emus)))->orWhere('username', trim($request->emus))->get();

        //dd($request->emus, strtolower(trim($request->emus)), $users->count());

        if ($users->count() != 1) {
            return $this->response(['message' => 'Invalid Credentials #' . $err_function_code . ($users->count() == 0 ? 'FIC' : 'ISECI') . '', 'error' => true, 'statusCode' => 401]);
        }

        $user = $users->first();

        if (!auth()->attempt(['email' => $user->email, 'password' => $request->scode])) {
            return $this->response(['message' => 'Invalid Credentials #' . $err_function_code . 'FIC.', 'error' => true, 'statusCode' => 401]);
        }

        //dd($user);

        $accessToken = auth()->user()->createToken($user->token_id)->accessToken;

        return $this->response(['message' => 'User logged successfully.', 'data' =>  new UserLoginResource($user, $accessToken), 'statusCode' => 200]);
        /*} catch (\Exception $ex) {
            return $this->response(['message' => 'User registered failed #' . $err_function_code . 'ISE.', 'error' => true, 'statusCode' => 500]);
        }*/
    }
}
