<?php

namespace App\Http\Controllers\Opened\Auth;

use App\Models\User;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\OpenedRegisterRequest;
use App\Http\Resources\UserRegisterResource;

class RegisterController extends Controller
{
    static public $err_class_code = "RO2000";
    public function register(OpenedRegisterRequest $request)
    {
        /*
         * LO2000-OP-ISE = INTERNAL SERVER ERROR
         */
        $err_function_code = self::$err_class_code . '-' . 'OP' . '-';

        try {

            $user = User::create([
                'forname' => trim($request->firstname), // Prénoms
                'surname' => trim($request->lastname), // Nom
                'email' => strtolower(trim($request->email)),
                'username' => trim($request->username),
                'phone' => trim($request->phone),
                'password' => bcrypt($request->scode),
                'ref' => trim($request->personrefkey),
                'ref_perfectmoney' => $request->perfect == null || trim($request->perfect) == "" ? null : trim($request->perfect),
                'sponsor_ref_at_inscription' => trim($request->sponsrefkey),
                'sponsor_id' => User::where('ref', trim($request->sponsrefkey))->first()->id,
                'token_id' => Uuid::uuid4()->toString() . time(),
            ]);

            return $this->response(['message' => 'User registered successfully.', 'data' =>  new UserRegisterResource($user), 'statusCode' => 201]);
        } catch (\Exception $ex) {
            return $this->response(['message' => 'User registered failed #' . $err_function_code . 'ISE. ' . json_encode($ex->getMessage()), 'error' => true, 'statusCode' => 500]);
        }
    }
}
