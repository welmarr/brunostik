<?php

namespace App\Http\Controllers\Opened;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\Opened\CommissionRepositoryInterface;

class CommissionController extends Controller
{
    protected $commissionRepository;

    public function __construct(CommissionRepositoryInterface $commissionRepository)
    {
        $this->commissionRepository = $commissionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->response($this->commissionRepository->getAll($request));
    }
    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request    $request
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->response($this->commissionRepository->getById($id));
    }
}
