<?php

namespace App\Http\Controllers\Opened\V2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\Opened\V2\BetRepositoryInterface;

class BetController extends Controller
{
    protected $betRepository;

    public function __construct(BetRepositoryInterface $betRepository)
    {
        $this->betRepository = $betRepository;
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request    $request
     * @param  \Date  $for
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $for)
    {
        return $this->response($this->betRepository->getByFor($request, $for));
    }
}
