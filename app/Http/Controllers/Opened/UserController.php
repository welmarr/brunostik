<?php

namespace App\Http\Controllers\Opened;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\Opened\UserRepositoryInterface;

class UserController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Display all users associate to user connected
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllChildren(Request $request)
    {
        return $this->response($this->userRepository->getAllChildren($request));
    }
}
