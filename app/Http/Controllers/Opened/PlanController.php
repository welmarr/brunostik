<?php

namespace App\Http\Controllers\Opened;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\Opened\PlanRepositoryInterface;

class PlanController extends Controller
{
    protected $planRepository;

    public function __construct(PlanRepositoryInterface $planRepository)
    {
        $this->planRepository = $planRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->response($this->planRepository->getAll($request));
    }
    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request    $request
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->response($this->planRepository->getById($id));
    }



    /**
     * Get Current Plan
     *
     *
     * @access  public
     */
    public function current()
    {
        return $this->response($this->planRepository->current());
    }
}
