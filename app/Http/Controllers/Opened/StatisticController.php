<?php

namespace App\Http\Controllers\Opened;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\Opened\StatisticRepositoryInterface;

class StatisticController extends Controller
{
    protected $statisticRepository;

    public function __construct(StatisticRepositoryInterface $statisticRepository)
    {
        $this->statisticRepository = $statisticRepository;
    }

    /**
     * Display a listing of the resource.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->response($this->statisticRepository->getGeneral());
    }
}
