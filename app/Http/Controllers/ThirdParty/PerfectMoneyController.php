<?php

namespace App\Http\Controllers\ThirdParty;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PerfectMoneyFailedRequest;
use App\Http\Requests\PerfectMoneySuccessRequest;
use App\Repositories\ThirdParty\PerfectMoneyRepository;

class PerfectMoneyController extends Controller
{
    static public $err_class_code = "PMT9000";
    protected $perfectMoneyRepository;

    public function __construct(PerfectMoneyRepository $perfectMoneyRepository)
    {
        $this->perfectMoneyRepository = $perfectMoneyRepository;
    }


    public function successTransaction(PerfectMoneySuccessRequest $request)
    {
        /*
         * ERROR CODE
         * PMT9000-TR-FACLC = FAILDED AFTER CALL LOG CREATE
         * PMT9000-TR-FWCLC = FAILDED WHEN CALL LOG CREATING
         */
        $err_function_code = self::$err_class_code . '-' . 'TR' . '-';

        $rslt = $this->perfectMoneyRepository->success($request);

        if ($rslt == 1) {
            //return redirect()->away('https://www.google.com/search?q=success');

            return redirect()->away('https://green-house-official.com/paiement/success');
        }
        //return redirect()->away('https://www.google.com/search?q=failed-' .  $err_function_code . ($rslt == -1 ? 'FWCLC' : 'FACLC'));

        return redirect()->away('https://green-house-official.com/paiement/failed');
    }

    public function failedTransaction(PerfectMoneyFailedRequest $request)
    {
        /*
         * ERROR CODE
         * PMT9000-FX-SCLC = SUCCESS CALL LOG CREATE
         * PMT9000-FX-FCLC = FAILDED CALL LOG CREATING
         */
        $err_function_code = self::$err_class_code . '-' . 'FX' . '-';

        $rslt = $this->perfectMoneyRepository->failed($request);

        //return redirect()->away('https://www.google.com/search?q=failed-' .  $err_function_code . ($rslt == 1 ? 'SCLC' : 'FCLC'));

        return redirect()->away('https://green-house-official.com/paiement/failed');
    }
}
