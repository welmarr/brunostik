<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Opened\BetController as BetControllerOpened;
use App\Http\Controllers\Secured\BetController as BetControllerSecured;
use App\Http\Controllers\Secured\TeamController as TeamControllerSecured;
use App\Http\Controllers\Secured\EventController as EventControllerSecured;
use App\Http\Controllers\Secured\SportController as SportControllerSecured;
use App\Http\Controllers\Opened\Auth\LoginController as LoginControllerOpened;
use App\Http\Controllers\Secured\PricingController as PricingControllerSecured;
use App\Http\Controllers\Secured\PronosticController as PronosticControllerSecured;
use App\Http\Controllers\Opened\Auth\RegisterController as RegisterControllerOpened;
use App\Http\Controllers\Secured\ChampionatController as ChampionatControllerSecured;
use App\Http\Controllers\ThirdParty\PerfectMoneyController as PerfectMoneyControllerThirdParty;
use App\Http\Controllers\Opened\UserController as UserControllerOpened;
use App\Http\Controllers\Opened\CommissionController as CommissionControllerOpened;
use App\Http\Controllers\Opened\PlanController as PlanControllerOpened;
use App\Http\Controllers\Opened\MoovMtnTransactionController as MoovMtnTransactionControllerOpened;
use App\Http\Controllers\Secured\MoovMtnTransactionController as MoovMtnTransactionControllerSecured;
use App\Http\Controllers\Secured\CommissionController as CommissionControllerSecured;
use App\Http\Controllers\Opened\StatisticController as StatisticController;
use App\Http\Controllers\Secured\V2\PronosticController as V2PronosticControllerSecured;
use App\Http\Controllers\Secured\V2\BetController as V2BetControllerSecured;
use App\Http\Controllers\Opened\V2\BetController as V2BetControllerOpened;
use App\Http\Controllers\Opened\Auth\V2\LoginController as V2LoginControllerOpened;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/tests', function () {
    return redirect()->away('https://www.google.com');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::resource('teams', TeamControllerSecured::class)->except(['create', 'edit']);
    Route::resource('pricings', PricingControllerSecured::class)->except(['create', 'edit']);
    Route::resource('sports', SportControllerSecured::class)->except(['create', 'edit']);
    Route::resource('championats', ChampionatControllerSecured::class)->except(['create', 'edit']);
    Route::resource('events', EventControllerSecured::class)->except(['create', 'edit']);
    Route::resource('bets', BetControllerSecured::class)->except(['create', 'edit']);
    Route::resource('pronostics', PronosticControllerSecured::class)->except(['create', 'edit']);


    // Route::resource('commissions', CommissionControllerSecured::class)->except(['create', 'edit', 'destroy', 'update', 'store']);
    Route::get('/commissions', [CommissionControllerSecured::class, 'index'])->name('commissions.index');
    Route::get('/commissions/{commission}', [CommissionControllerSecured::class, 'show'])->name('commissions.show')->whereUuid('commission');
    Route::get('/commissions/validated', [CommissionControllerSecured::class, 'validated'])->name('commissions.validated');
    Route::get('/commissions/rejected', [CommissionControllerSecured::class, 'rejected'])->name('commissions.rejected');


    Route::group(['prefix' => 'v2', 'as' => 'v2.'], function () {
        Route::resource('pronostics', V2PronosticControllerSecured::class)->except(['create', 'edit']);
        Route::resource('bets', V2BetControllerSecured::class)->only(['index', 'show']);
    });
    Route::group(['prefix' => 'deposit',  'as' => 'deposit.'], function () {
        Route::resource('moov-or-mtn', MoovMtnTransactionControllerSecured::class)->except(['create', 'edit', 'update']);
    });
});





Route::group(['prefix' => 'open', 'as' => 'open.'], function () {

    Route::middleware('auth:api')->group(function () {

        Route::resource('bets', BetControllerOpened::class)->except(['create', 'edit', 'destroy', 'update', 'store'])->parameters([
            'bets' => 'for'
        ])->names([
            'index' => 'bets.index',
            'show' => 'bets.show',
        ]);

        // Route::resource('plans', PlanControllerOpened::class)->except(['create', 'edit', 'destroy', 'update', 'store']);
        Route::get('/plans', [PlanControllerOpened::class, 'index'])->name('plans.index');
        Route::get('/plans/{plan}', [PlanControllerOpened::class, 'show'])->name('plans.show')->whereUuid('plan');
        Route::get('/plans/current', [PlanControllerOpened::class, 'current'])->name('plans.current');

        Route::get('/statistics', [StatisticController::class, 'index'])->name('statistics.index');

        // Route::resource('commissions', CommissionControllerOpened::class)->except(['create', 'edit', 'destroy', 'update', 'store']);
        Route::get('/commissions', [CommissionControllerOpened::class, 'index'])->name('commissions.index');
        Route::get('/commissions/{commission}', [CommissionControllerOpened::class, 'show'])->name('commissions.show')->whereUuid('commission');


        Route::group(['prefix' => 'deposit',  'as' => 'deposit.'], function () {
            Route::resource('moov-or-mtn', MoovMtnTransactionControllerOpened::class)->only(['index', 'store']);
        });

        Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
            Route::get('/children', [UserControllerOpened::class, 'getAllChildren'])->name('all.children');
        });


        Route::group(['prefix' => 'v2', 'as' => 'v2.'], function () {
            Route::resource('bets', V2BetControllerOpened::class)->only(['show'])->parameters([
                'bets' => 'for'
            ])->names([
                'show' => 'bets.show',
            ]);
        });
    });


    Route::prefix('auth')->group(function () {
        Route::post('/register', [RegisterControllerOpened::class, 'register'])->name('open.auth.register');
        Route::post('/login', [LoginControllerOpened::class, 'login'])->name('open.auth.login');
    });

    Route::group(['prefix' => 'auth', 'as' => 'auth.'], function () {
        Route::post('/register', [RegisterControllerOpened::class, 'register'])->name('register');
        Route::post('/login', [LoginControllerOpened::class, 'login'])->name('login');
    });

    Route::group(['prefix' => 'v2', 'as' => 'v2.'], function () {
        Route::group(['prefix' => 'auth', 'as' => 'auth.'], function () {
            Route::post('/login', [V2LoginControllerOpened::class, 'login'])->name('login');
        });
    });
});



Route::group(['prefix' => 'third-party',  'as' => 'third.party.'], function () {
    Route::post('/success', [PerfectMoneyControllerThirdParty::class, 'successTransaction'])->name('third.party.success');
    Route::post('/failed', [PerfectMoneyControllerThirdParty::class, 'failedTransaction'])->name('third.party.failed');
});
